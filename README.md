# Exploring the gender gap in science: number of Wikidata records about female vs male scientists

_Using the SPARQL Jupyter kernel to query Wikidata_

A notebook by Odile Bénassy and Samuel Lelièvre, prepared on 2019-02-13 at the conference: [Calcul mathématique libre](https://opendreamkit.org/2019/02/11/FreeComputationalMathematicsConference/) (11-15 février 2019, CIRM, Marseille, France)

During the conference [Calcul mathématique libre](https://opendreamkit.org/2019/02/11/FreeComputationalMathematicsConference/) (11-15 février 2019, CIRM, Marseille, France), an evening session on Women in Science was held on Wednesday evening, with three presentations:
- Marie-Françoise Roy presented the [Committee for Women in Mathematics](www.mathunion.org/cwm)
- Odile Bénassy presented a demo of using SPARQL (the present document)
- Viviane Pons presented Women in Sage days, PyLadies Paris

## How to get information

Wikidata is the place where all basic Wikipedia information is stored, for example the scientists' names, dates of birth, research fields, ... and gender.

We use SPARQL queries, through the Jupyter SPARQL kernel, to retrieve this information from Wikidata.


## Installing

https://github.com/paulovn/sparql-kernel

```
pip install sparqlkernel --user
```

```
jupyter sparqlkernel install --user
```

For installing system-wide, do instead

```
jupyter sparqlkernel install --sys-prefix
```
